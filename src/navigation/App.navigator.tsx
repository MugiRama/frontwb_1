// src/navigation/AppNavigator.tsx
import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { RootStackParamList } from './navigationTypes';
import LoginScreen from 'src/Screens/LoginScreen';
import RegisterScreen from 'src/Screens/RegisterScreen';
import WelcomeScreen from 'src/Screens/WelcomeScreen';
import PasswordRecoveryScreen from 'src/Screens/PasswordRecoveryScreen';
import AttendanceScreen from 'src/Screens/AttendanceScreen';
import ProfileAdminScreen from 'src/Screens/ProfileAdminScreen';
import ProfileScreen from 'src/Screens/ProfileScreen';
import WeeklySummaryScreen from 'src/Screens/WeeklySummaryScreen';
import WorkerListScreen from 'src/Screens/WorkerListScreen';
import WeeklyChartsScreen from 'src/Screens/WeeklyChartsScreen';
import AnnualChartsScreen from 'src/Screens/AnnualChartsScreen';
import ChartsScreen from 'src/Screens/ChartsScreen';
import WeeklyChartsMultipleScreen from 'src/Screens/WeeklyChartsMultipleScreen';
import WeeklySummaryByEmailScreen from 'src/Screens/WeeklySummaryByEmailScreen';
import PasswordChangeScreen from 'src/Screens/PasswordChangeScreen';




const Stack = createNativeStackNavigator<RootStackParamList>();

function AppNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Login" component={LoginScreen} options={{ title: 'Login' }} />
      <Stack.Screen name="Register" component={RegisterScreen} options={{ title: 'Registro' }} />
      <Stack.Screen name="Welcome" component={WelcomeScreen} options={{ title: 'Welcome' }} />
      <Stack.Screen name="PasswordRecovery" component={PasswordRecoveryScreen} options={{ title: 'Recuperar Contraseña' }} />
      <Stack.Screen name="Attendance" component={AttendanceScreen} options={{ title: 'Marcar Asistencia' }} />
      <Stack.Screen name="ProfileAdmin" component={ProfileAdminScreen} options={{ title: 'Perfil Admin' }} />
      <Stack.Screen name="Profile" component={ProfileScreen} options={{ title: 'Perfil' }} />
      <Stack.Screen name="WeeklySummary" component={WeeklySummaryScreen} options={{ title: 'mi resumen semanal' }} />
      <Stack.Screen name="WorkerList" component={WorkerListScreen} options={{ title: 'lista de trabajadores' }} />
      <Stack.Screen name="WeeklyCharts" component={WeeklyChartsScreen} options={{ title: 'Gráfico semanal' }} />
      <Stack.Screen name="Charts" component={ChartsScreen} options={{ title: 'Gráficos ' }} />
      <Stack.Screen name="AnnualCharts" component={AnnualChartsScreen} options={{ title: 'Gráficos Anuales ' }} />
      <Stack.Screen name="WeeklyChartsMultiple" component={WeeklyChartsMultipleScreen} options={{ title: 'Gráficos Semanal Múltiple' }} />
      <Stack.Screen name="WeeklySummaryByEmail" component={WeeklySummaryByEmailScreen} options={{ title: 'resumen semanal' }} />
      <Stack.Screen name="PasswordChange" component={PasswordChangeScreen} options={{ title: 'Cambiar Contraseña' }} />
    </Stack.Navigator>
  );
}

export default AppNavigator;
