// src/navigation/navigationTypes.ts
import { RouteProp } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';

export type RootStackParamList = {
  Login: undefined;
  Welcome: undefined;
  Register: undefined;
  PasswordRecovery: undefined;
  Attendance: undefined;
  Profile: undefined;
  LoginAdmin: undefined;
  ProfileAdmin: undefined;
  ProfileEdit: undefined;
  WorkerList: undefined;
  WorkerEmail: undefined;
  WeeklySummary: undefined;
  WeeklyCharts: undefined;
  Charts:undefined;
  AnnualCharts:undefined;
  WeeklyChartsMultiple:undefined;
  AnnualChartsMultiple: undefined;
  WeeklySummaryByEmail: { workerEmail: string }
  PasswordChange: undefined;

};

export type LoginScreenNavigationProp = NativeStackNavigationProp<RootStackParamList, 'Login'>;
export type RegisterScreenNavigationProp = NativeStackNavigationProp<RootStackParamList, 'Register'>;
export type PasswordRecoveryScreenNavigationProp = NativeStackNavigationProp<RootStackParamList, 'PasswordRecovery'>;
export type ProfileAdminScreenNavigationProp = NativeStackNavigationProp<RootStackParamList, 'ProfileAdmin'>;
export type ProfileScreenNavigationProp = NativeStackNavigationProp<RootStackParamList, 'Profile'>;
export type AttendanceScreenNavigationProp = NativeStackNavigationProp<RootStackParamList, 'Attendance'>;
export type ProfileEditScreenNavigationProp = NativeStackNavigationProp<RootStackParamList, 'ProfileEdit'>;
export type WeeklySummaryScreenNavigationProp = NativeStackNavigationProp<RootStackParamList, 'WeeklySummary'>; 
export type WorkerListScreenNavigationProp = NativeStackNavigationProp<RootStackParamList, 'WorkerList'>;
export type WeeklySummaryScreenRouteProp = RouteProp<RootStackParamList,'WeeklySummary'>;
export type WeeklyChartsScreenNavigationProp = NativeStackNavigationProp<RootStackParamList, 'WeeklyCharts'>;
export type ChartsScreenNavigationProp = NativeStackNavigationProp<RootStackParamList, 'Charts'>;
export type AnnualChartsScreenNavigationProp = NativeStackNavigationProp<RootStackParamList, 'AnnualCharts'>;
export type WeeklyChartsMultipleScreenNavigationProp = NativeStackNavigationProp<RootStackParamList, 'WeeklyChartsMultiple'>;
export type AnnualChartsMultipleScreenNavigationProp = NativeStackNavigationProp<RootStackParamList, 'AnnualChartsMultiple'>;
export type WeeklySummaryScreenByEmailRouteProp = RouteProp<RootStackParamList,'WeeklySummaryByEmail'>;
export type PasswordChangeScreenNavigationProp = NativeStackNavigationProp<RootStackParamList, 'PasswordChange'>;