

export interface Attendance{
    id: number;
    userEmail: string;
    timestamp: string;
    isEntry: boolean;
    latitude: number;
    longitude: number;
    isPlaceholder?: boolean;
}

export type AttendanceData = { [date: string]: Attendance[] };
