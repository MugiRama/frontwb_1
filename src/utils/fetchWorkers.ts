import AsyncStorage from '@react-native-async-storage/async-storage';
import { Alert } from 'react-native';
import {API_USER_URL} from "@env"

export type Worker = {
  id: number;
  name: string;
  email: string;
};

export const fetchWorkers = async (): Promise<Worker[]> => {
  try {
    const token = await AsyncStorage.getItem('userToken');
    if (!token) {
      Alert.alert('Error de sesión', 'No se encontró el token de sesión. Por favor, inicie sesión nuevamente.');
      return [];
    }

    const response = await fetch(`${API_USER_URL}/users`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
      },
    });

    if (!response.ok) {
      throw new Error('Error al obtener la lista de trabajadores');
    }

    const data = await response.json();
    return data;
  } catch (error) {
    const errorMessage = error instanceof Error ? error.message : 'Error al obtener trabajadores';
    Alert.alert('Error', errorMessage);
    return [];
  }
};
