import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, Button, ScrollView, Alert, StyleSheet, FlatList, TouchableOpacity, Dimensions } from 'react-native';
import { LineChart } from 'react-native-chart-kit';
import { fetchWorkers, Worker } from '../utils/fetchWorkers';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {API_ATTENDANCE_URL} from "@env"

const screenWidth = Dimensions.get("window").width;
const monthLabels = ['E', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D'];

const AnnualChartsScreen = () => {
    const [token, setToken] = useState('');
    const [email, setEmail] = useState('');
    const [selectedWorker, setSelectedWorker] = useState<Worker | null>(null);
    const [year, setYear] = useState(new Date().getFullYear());
    const [monthlyData, setMonthlyData] = useState<number[]>([]);
    const [workers, setWorkers] = useState<Worker[]>([]);
    const [filteredWorkers, setFilteredWorkers] = useState<Worker[]>([]);
    const [searchEmail, setSearchEmail] = useState('');

    useEffect(() => {
        const fetchData = async () => {
            const storedToken = await AsyncStorage.getItem('userToken');
            if (storedToken) {
                setToken(storedToken);
                const workersData = await fetchWorkers();
                setWorkers(workersData);
                setFilteredWorkers(workersData);
            }
        };

        fetchData();
    }, []);

    useEffect(() => {
        if (token && email) {
            fetchMonthlyHours();
        }
    }, [year, token, email]);

    const fetchMonthlyHours = async () => {
        if (!email) {
            Alert.alert('Error', 'Por favor, ingrese un correo electrónico válido.');
            return;
        }

        try {
            const response = await fetch(`${API_ATTENDANCE_URL}/attendance/monthly-hours?email=${email}&year=${year}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                }
            });

            if (!response.ok) {
                throw new Error(`HTTP error! status: ${response.status}`);
            }

            const responseData = await response.json();

            if (typeof responseData !== 'object' || responseData === null) {
                throw new Error('La respuesta no es un objeto.');
            }

            const dataPoints = monthLabels.map((_, index) => Number(responseData[Object.keys(responseData)[index]]) || 0);
            setMonthlyData(dataPoints);

        } catch (error) {
            const errorMessage = error instanceof Error ? error.message : 'Error al obtener los datos mensuales';
            Alert.alert('Error', errorMessage);
        }
    };

    useEffect(() => {
        setFilteredWorkers(
            workers.filter((worker) =>
                worker.email.toLowerCase().includes(searchEmail.toLowerCase())
            )
        );
    }, [searchEmail, workers]);

    const handleSelectWorker = (worker: Worker) => {
        setEmail(worker.email);
        setSelectedWorker(worker);
        setSearchEmail('');
    };

    const chartData = {
        labels: monthLabels,
        datasets: [{
            data: monthlyData,
            color: (opacity = 1) => `rgba(134, 65, 244, ${opacity})`,
            strokeWidth: 2
        }]
    };

    return (
        <ScrollView>
            <View style={{ padding: 20 }}>
                <Text>Buscar Trabajador por Email:</Text>
                <TextInput
                    style={{ borderColor: 'gray', borderWidth: 1, marginBottom: 10, padding: 10 }}
                    value={searchEmail}
                    onChangeText={setSearchEmail}
                    placeholder="Ingrese el correo electrónico"
                />
                {searchEmail.length > 0 && (
                    <FlatList
                        data={filteredWorkers}
                        keyExtractor={(item) => item.id.toString()}
                        renderItem={({ item }) => (
                            <TouchableOpacity onPress={() => handleSelectWorker(item)}>
                                <Text style={[styles.workerText, selectedWorker?.email === item.email && styles.selectedWorkerText]}>
                                    {item.email}
                                </Text>
                            </TouchableOpacity>
                        )}
                    />
                )}
                {selectedWorker && (
                    <Text style={styles.selectedWorkerText}>Trabajador Seleccionado: {selectedWorker.name} ({selectedWorker.email})</Text>
                )}
                <View style={styles.buttonContainer}>
                    <Button title="Buscar" onPress={fetchMonthlyHours} color="#800080" />
                </View>
                <View style={styles.navigationContainer}>
                    <View style={styles.buttonWrapper}>
                        <Button title="Año Anterior" onPress={() => setYear(year - 1)} color="#800080" />
                    </View>
                    <Text style={styles.yearText}>{year}</Text>
                    <View style={styles.buttonWrapper}>
                        <Button title="Año Siguiente" onPress={() => setYear(year + 1)} color="#800080" />
                    </View>
                </View>
                {monthlyData.length > 0 && (
                    <LineChart
                        data={chartData}
                        width={screenWidth - 40}
                        height={220}
                        chartConfig={{
                            backgroundColor: '#1cc910',
                            backgroundGradientFrom: '#eff3ff',
                            backgroundGradientTo: '#efefef',
                            decimalPlaces: 0,
                            color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                            labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                            style: {
                                borderRadius: 16
                            },
                            propsForDots: {
                                r: "6",
                                strokeWidth: "2",
                                stroke: "#ffa726"
                            }
                        }}
                        bezier
                        style={{
                            marginVertical: 8,
                            borderRadius: 16
                        }}
                    />
                )}
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    workerText: {
        fontSize: 16,
        padding: 10,
    },
    selectedWorkerText: {
        fontSize: 16,
        fontWeight: 'bold',
        padding: 10,
        marginTop: 10,
        backgroundColor: '#f0f0f0',
        borderRadius: 5,
    },
    buttonContainer: {
        marginVertical: 10,
    },
    navigationContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: 10,
    },
    buttonWrapper: {
        flex: 1,
        marginHorizontal: 5,
    },
    yearText: {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
    },
});
export default AnnualChartsScreen;
