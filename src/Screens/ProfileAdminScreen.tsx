import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { styles } from '../components/styles/GlobalStyles';
import { ProfileAdminScreenNavigationProp } from '../navigation/navigationTypes';

interface ProfileAdminScreenProps {
  navigation: ProfileAdminScreenNavigationProp;
}

function ProfileAdminScreen({ navigation }: ProfileAdminScreenProps) {

  const goToWorkerList = () => {
    navigation.navigate('WorkerList');
  };
  const goToChart = () => {
    navigation.navigate('Charts');
  };

  return (
    <View style={styles.container}>
      <Text style={styles.titleText}>Bienvenido Administrador</Text>
      <TouchableOpacity style={styles.button} onPress={goToWorkerList}>
        <Text style={styles.buttonText}>Ver Lista de Trabajadores</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={goToChart}>
        <Text style={styles.buttonText}>Ver Gráficos </Text>
      </TouchableOpacity>
    </View>
  );
}

export default ProfileAdminScreen;