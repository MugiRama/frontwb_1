// WeeklyChartsScreen.tsx
import React, { useEffect, useState } from 'react';
import { View, Text, TextInput, Button, Alert, StyleSheet, FlatList, TouchableOpacity, Dimensions } from 'react-native';
import { BarChart } from 'react-native-chart-kit';
import { fetchWorkers, Worker } from '../utils/fetchWorkers';
import AsyncStorage from '@react-native-async-storage/async-storage';

const screenWidth = Dimensions.get("window").width;

type WeeklyData = {
  Monday: number;
  Tuesday: number;
  Wednesday: number;
  Thursday: number;
  Friday: number;
  Saturday: number;
  Sunday: number;
};

const WeeklyChartsScreen = () => {
  const [token, setToken] = useState('');
  const [email, setEmail] = useState('');
  const [selectedWorker, setSelectedWorker] = useState<Worker | null>(null);
  const [data, setData] = useState<WeeklyData | null>(null);
  const [weekOffset, setWeekOffset] = useState(0);
  const [chartData, setChartData] = useState({
    labels: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
    datasets: [
      {
        data: [0, 0, 0, 0, 0, 0, 0]
      }
    ]
  });
  const [workers, setWorkers] = useState<Worker[]>([]);
  const [filteredWorkers, setFilteredWorkers] = useState<Worker[]>([]);
  const [searchEmail, setSearchEmail] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      const storedToken = await AsyncStorage.getItem('userToken');
      if (storedToken) {
        setToken(storedToken);
        const workersData = await fetchWorkers();
        setWorkers(workersData);
        setFilteredWorkers(workersData);
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    if (data) {
      const orderedData = [
        data.Monday || 0,
        data.Tuesday || 0,
        data.Wednesday || 0,
        data.Thursday || 0,
        data.Friday || 0,
        data.Saturday || 0,
        data.Sunday || 0
      ];

      const formattedData = {
        labels: ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
        datasets: [
          {
            data: orderedData
          }
        ]
      };
      setChartData(formattedData);
    }
  }, [data]);

  const fetchWeeklyData = async () => {
    if (!email) {
      Alert.alert('Error', 'Por favor, ingrese un correo electrónico válido.');
      return;
    }

    try {
      const response = await fetch(`http://192.168.0.7:3009/api/v3/attendance/weekly-hours?email=${email}&weekOffset=${weekOffset}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        }
      });

      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }

      const responseData = await response.json();

      if (typeof responseData !== 'object' || responseData === null) {
        throw new Error('La respuesta no es un objeto.');
      }

      const formattedData: WeeklyData = {
        Monday: responseData.Monday?.totalHours || 0,
        Tuesday: responseData.Tuesday?.totalHours || 0,
        Wednesday: responseData.Wednesday?.totalHours || 0,
        Thursday: responseData.Thursday?.totalHours || 0,
        Friday: responseData.Friday?.totalHours || 0,
        Saturday: responseData.Saturday?.totalHours || 0,
        Sunday: responseData.Sunday?.totalHours || 0,
      };

      setData(formattedData);
    } catch (error) {
      const errorMessage = error instanceof Error ? error.message : 'Error ';
      Alert.alert('Error al obtener los datos semanales', errorMessage);
    }
  };

  const navigateWeek = (direction: string) => {
    setWeekOffset(prev => direction === 'previous' ? prev - 1 : prev + 1);
  };

  useEffect(() => {
    if (email) {
      fetchWeeklyData();
    }
  }, [weekOffset]);

  useEffect(() => {
    setFilteredWorkers(
      workers.filter((worker) =>
        worker.email.toLowerCase().includes(searchEmail.toLowerCase())
      )
    );
  }, [searchEmail, workers]);

  const handleSelectWorker = (worker: Worker) => {
    setEmail(worker.email);
    setSelectedWorker(worker);
    setSearchEmail('');
  };

  return (
    <View style={styles.container}>
      <View style={styles.contentContainer}>
        <Text style={styles.titleText}>Buscar Trabajador por Email:</Text>
        <TextInput
          style={styles.input}
          placeholder="Buscar por email"
          value={searchEmail}
          onChangeText={setSearchEmail}
        />
        {searchEmail.length > 0 && (
          <FlatList
            data={filteredWorkers}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item }) => (
              <TouchableOpacity onPress={() => handleSelectWorker(item)}>
                <Text style={[styles.workerText, selectedWorker?.email === item.email && styles.selectedWorkerText]}>
                  {item.email}
                </Text>
              </TouchableOpacity>
            )}
          />
        )}
        {selectedWorker && (
          <Text style={styles.selectedWorkerText}>Trabajador Seleccionado: {selectedWorker.name} ({selectedWorker.email})</Text>
        )}
        <View style={styles.buttonContainer}>
          <Button title="Buscar" onPress={fetchWeeklyData} color="#800080" />
        </View>
        <View style={styles.navigationContainer}>
          <View style={styles.buttonWrapper}>
            <Button title="Semana Anterior" onPress={() => navigateWeek('previous')} color="#800080" />
          </View>
          <View style={styles.buttonWrapper}>
            <Button title="Siguiente Semana" onPress={() => navigateWeek('next')} color="#800080" />
          </View>
        </View>
        {data && (
          <BarChart
            data={chartData}
            width={screenWidth - 40}
            height={400}
            yAxisLabel=""
            yAxisSuffix="h"
            yAxisInterval={1} 
            fromZero={true}
            chartConfig={{
              backgroundColor: '#1cc910',
              backgroundGradientFrom: '#eff3ff',
              backgroundGradientTo: '#efefef',
              decimalPlaces: 1,
              color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
              labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
              style: {
                borderRadius: 16
              },
              propsForDots: {
                r: "6",
                strokeWidth: "2",
                stroke: "#ffa726"
              },
              barPercentage: 0.5,
              fillShadowGradient: '#000',
              fillShadowGradientOpacity: 0.1
            }}
            verticalLabelRotation={90}
            style={{
              marginVertical: 8,
              borderRadius: 16
            }}
          />
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    padding: 20,
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 10,
    padding: 10,
  },
  workerText: {
    fontSize: 16,
    padding: 10,
  },
  selectedWorkerText: {
    fontSize: 16,
    fontWeight: 'bold',
    padding: 10,
    marginTop: 10,
    backgroundColor: '#f0f0f0',
    borderRadius: 5,
  },
  buttonContainer: {
    marginVertical: 10,
  },
  navigationContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  buttonWrapper: {
    flex: 1,
    marginHorizontal: 5,
  }
});

export default WeeklyChartsScreen;
