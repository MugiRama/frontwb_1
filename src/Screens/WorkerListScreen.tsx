import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, TouchableOpacity, Alert, TextInput } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';
import { WorkerListScreenNavigationProp } from '../navigation/navigationTypes';
import { styles } from '../components/styles/GlobalStyles';
import {API_USER_URL} from "@env"

interface Worker {
  id: number;
  name: string;
  email: string;
}

function WorkerListScreen() {
  const navigation = useNavigation<WorkerListScreenNavigationProp>();
  const [workers, setWorkers] = useState<Worker[]>([]);
  const [filteredWorkers, setFilteredWorkers] = useState<Worker[]>([]);
  const [searchEmail, setSearchEmail] = useState('');
  const [token, setToken] = useState('');

  useEffect(() => {
    const fetchTokenAndWorkers = async () => {
      try {
        const token = await AsyncStorage.getItem('userToken');
        if (!token) {
          Alert.alert('Error de sesión', 'No se encontró el token de sesión. Por favor, inicie sesión nuevamente.');
          return;
        }
        setToken(token);

        const response = await fetch(`${API_USER_URL}/users`,{
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
          },
        });

        if (!response.ok) {
          const errorData = await response.json();
          throw new Error(errorData.message || 'Error al obtener la lista de trabajadores');
        }

        const data = await response.json();
        setWorkers(data);
        setFilteredWorkers(data);
      } catch (error) {
        const errorMessage = error instanceof Error ? error.message : 'Error ';
        Alert.alert('Error al obtener trabajadores', errorMessage);
      }
    };

    fetchTokenAndWorkers();
  }, []);

  useEffect(() => {
    setFilteredWorkers(
      workers.filter((worker) =>
        worker.email.toLowerCase().includes(searchEmail.toLowerCase())
      )
    );
  }, [searchEmail, workers]);

  const handlePress = (worker: Worker) => {
    navigation.navigate('WeeklySummaryByEmail', { workerEmail: worker.email });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.titleText}>Lista de Trabajadores</Text>
      <TextInput
        style={styles.input}
        placeholder="Buscar por email"
        value={searchEmail}
        onChangeText={setSearchEmail}
      />
      <FlatList
        data={filteredWorkers}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <TouchableOpacity style={styles.workerButton} onPress={() => handlePress(item)}>
            <Text style={styles.workerButtonText}>Nombre: {item.name}</Text>
            <Text style={styles.workerButtonText}>Email: {item.email}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
}

export default WorkerListScreen;
