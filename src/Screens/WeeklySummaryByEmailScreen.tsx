import React, { useEffect, useState } from 'react';
import { View, Text, ScrollView, Alert, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useRoute } from '@react-navigation/native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { WeeklySummaryScreenByEmailRouteProp } from '../navigation/navigationTypes';
import { Attendance } from '../types/attendanceTypes';
import {API_ATTENDANCE_URL} from "@env"

const WeeklySummaryByEmailScreen = () => {
  const route = useRoute<WeeklySummaryScreenByEmailRouteProp>();
  const { workerEmail } = route.params;

  const [weekOffset, setWeekOffset] = useState(0);
  const [attendances, setAttendances] = useState<{ [date: string]: Attendance[] }>({});
  const [loading, setLoading] = useState(false);
  const [editMode, setEditMode] = useState<{ [id: number]: boolean }>({});
  const [editedAttendances, setEditedAttendances] = useState<{ [id: number]: Partial<Attendance> }>({});
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [currentEditId, setCurrentEditId] = useState<number | null>(null);

  useEffect(() => {
    if (workerEmail) {
      fetchAttendances();
    }
  }, [weekOffset, workerEmail]);

  const fetchAttendances = async () => {
    setLoading(true);
    const token = await AsyncStorage.getItem('userToken');
    if (!token) {
      Alert.alert('Error', 'No hay token de usuario almacenado');
      setLoading(false);
      return;
    }

    try {
      const response = await fetch(`${API_ATTENDANCE_URL}/attendance/weekly-summary-email?email=${workerEmail}&weekOffset=${weekOffset}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`
        }
      });

      if (!response.ok) {
        throw new Error('Failed to fetch data');
      }

      const data = await response.json();
      setAttendances(data);
    } catch (error) {
      const errorMessage = error instanceof Error ? error.message : 'Error al obtener los datos';
      Alert.alert('Error', errorMessage);
    } finally {
      setLoading(false);
    }
  };

  const getDaysOfWeek = (weekOffset: number) => {
    const days = [];
    const currentDate = new Date();
    currentDate.setUTCDate(currentDate.getUTCDate() + weekOffset * 7);
  
    const monday = new Date(Date.UTC(currentDate.getUTCFullYear(), currentDate.getUTCMonth(), currentDate.getUTCDate()));
    monday.setUTCDate(monday.getUTCDate() - (monday.getUTCDay() + 6) % 7);
  
    for (let i = 0; i < 7; i++) {
      const day = new Date(Date.UTC(monday.getUTCFullYear(), monday.getUTCMonth(), monday.getUTCDate() + i));
      days.push(day.toISOString().split('T')[0]);
    }
  
    return days;
  };
  
  const formatDay = (day: string) => {
    const date = new Date(day);
    const options: Intl.DateTimeFormatOptions = {
      weekday: 'long',
      year: 'numeric',
      month: 'long',
      day: 'numeric',
      timeZone: 'UTC'
    };
    return date.toLocaleDateString('es-ES', options);
  };

  const navigateWeek = (direction: 'previous' | 'next') => {
    setWeekOffset(prev => direction === 'previous' ? prev - 1 : prev + 1);
  };

  const toggleEditMode = (id: number) => {
    setEditMode(prev => ({ ...prev, [id]: !prev[id] }));
    if (!editMode[id]) {
      const attendanceEntry = Object.values(attendances).flat().find(entry => entry.id === id);
      if (attendanceEntry) {
        setEditedAttendances(prev => ({ ...prev, [id]: attendanceEntry }));
      }
    }
  };

  const handleEditChange = (id: number, field: keyof Attendance, value: any) => {
    setEditedAttendances(prev => ({
      ...prev,
      [id]: {
        ...prev[id],
        [field]: value,
      },
    }));
  };
  
  const showDatePicker = (id: number) => {
    setCurrentEditId(id);
    setDatePickerVisibility(true);
  };
  
  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };
  
  const handleConfirm = (time: Date, id: number) => {
    const originalTimestamp = new Date(editedAttendances[id]?.timestamp || new Date());
    const newTimestamp = new Date(originalTimestamp);
    newTimestamp.setHours(time.getHours());
    newTimestamp.setMinutes(time.getMinutes());
    handleEditChange(id, 'timestamp', newTimestamp);
    hideDatePicker();
  };
  
  const saveAttendance = async (id: number) => {
    const token = await AsyncStorage.getItem('userToken');
    if (!token) {
      Alert.alert('Error', 'No hay token de usuario almacenado');
      return;
    }
  
    const { timestamp, isEntry } = editedAttendances[id];
    const isAdmin = true;  
    try {
      const response = await fetch(`${API_ATTENDANCE_URL}/attendance/update/${id}`, {
        method: 'PATCH',
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ timestamp, isEntry, isAdmin }),
      });
  
      if (!response.ok) {
        const errorText = await response.text();
        throw new Error(`Failed to update data: ${response.status} - ${errorText}`);
      }
  
      Alert.alert('Editado correctamente');
      setEditMode(prev => ({ ...prev, [id]: false }));
      fetchAttendances();
    } catch (error) {
      const errorMessage = error instanceof Error ? error.message : 'Error al actualizar los datos';
      Alert.alert('Error', errorMessage);
    }
  };
  
  const renderDayAttendances = (day: string) => {
    const dayAttendances = attendances[day];
    if (!dayAttendances || dayAttendances.length === 0) {
      return <Text style={styles.normalText}>No hay registros</Text>;
    }
  
    return dayAttendances.map((entry: Attendance) => (
      <View key={entry.id} style={styles.entryContainer}>
        {editMode[entry.id] ? (
          <>
            <TouchableOpacity style={styles.purpleButton} onPress={() => showDatePicker(entry.id)}>
              <Text style={styles.buttonText}>Seleccionar Hora</Text>
            </TouchableOpacity>
            <DateTimePicker
              isVisible={isDatePickerVisible}
              mode="time"
              onConfirm={(time) => handleConfirm(time, entry.id)}
              onCancel={hideDatePicker}
            />
            <TouchableOpacity style={styles.purpleButton} onPress={() => saveAttendance(entry.id)}>
              <Text style={styles.buttonText}>Guardar</Text>
            </TouchableOpacity>
          </>
        ) : (
          <>
            <Text style={styles.normalText}>
              Hora: {new Date(entry.timestamp).toLocaleTimeString()} - {' '}
              Tipo: {entry.isEntry ? 'Entrada' : 'Salida'}
            </Text>
            <TouchableOpacity style={styles.purpleButton} onPress={() => toggleEditMode(entry.id)}>
              <Text style={styles.buttonText}>Editar</Text>
            </TouchableOpacity>
          </>
        )}
      </View>
    ));
  };
  
  return (
    <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
      <Text style={styles.titleText}>Resumen Semanal de Asistencia para {workerEmail}</Text>
      {loading && <Text>Cargando...</Text>}
      {!loading && (
        <View style={styles.tableContainer}>
          <View style={styles.tableRow}>
            <Text style={styles.tableHeader}>Día</Text>
            <Text style={styles.tableHeader}>Hora</Text>
            <Text style={styles.tableHeader}>Tipo</Text>
            <Text style={styles.tableHeader}>Acciones</Text>
          </View>
          {getDaysOfWeek(weekOffset).map((day) => (
            <View key={day}>
              <Text style={styles.headerText}>{formatDay(day)}</Text>
              {renderDayAttendances(day)}
            </View>
          ))}
        </View>
      )}
      <View style={styles.navigationButtons}>
        <TouchableOpacity style={styles.purpleButton} onPress={() => navigateWeek('previous')}>
          <Text style={styles.buttonText}>Semana Anterior</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.purpleButton} onPress={() => navigateWeek('next')}>
          <Text style={styles.buttonText}>Siguiente Semana</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    padding: 20,
    alignItems: 'center',
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 20,
  },
  normalText: {
    fontSize: 16,
    marginVertical: 5,
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 10,
    padding: 10,
    width: '100%',
  },
  purpleButton: {
    backgroundColor: '#800080',
    padding: 10,
    borderRadius: 5,
    marginVertical: 5,
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
  },
  tableContainer: {
    width: '100%',
    marginTop: 20,
  },
  tableRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    paddingVertical: 10,
  },
  tableHeader: {
    fontWeight: 'bold',
    fontSize: 16,
    width: '25%',
    textAlign: 'center',
  },
  navigationButtons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    marginTop: 20,
  },
  entryContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    paddingVertical: 10,
  },
});

export default WeeklySummaryByEmailScreen;
