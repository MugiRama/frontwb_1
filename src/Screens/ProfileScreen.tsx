import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useEffect, useState } from 'react';
import { Alert, TextInput, TouchableOpacity, View, Text, ActivityIndicator } from 'react-native';
import { styles } from '../components/styles/GlobalStyles';
import { ProfileScreenNavigationProp } from '../navigation/navigationTypes';
import {API_USER_URL} from "@env"

interface ProfileScreenProps {
  navigation: ProfileScreenNavigationProp;
}

function ProfileScreen({ navigation }: ProfileScreenProps) {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [role, setRole] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [userToken, setUserToken] = useState('');
  const [isEditing, setIsEditing] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const loadData = async () => {
      const token = await AsyncStorage.getItem('userToken');
      if (token) {
        setUserToken(token);
        try {
          const response = await fetch(`${API_USER_URL}/users/profile`, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`
            }
          });
          if (response.ok) {
            const userData = await response.json();
            setName(userData.name);
            setEmail(userData.email);
            setRole(userData.role);
            setPhoneNumber(userData.phoneNumber);
          } else {
            throw new Error('Error al obtener los datos del usuario.');
          }
        } catch (error) {
          const errorMessage = error instanceof Error ? error.message : 'Error desconocido';
          Alert.alert('Error', errorMessage);
        } finally {
          setLoading(false);
        }
      } else {
        Alert.alert('Error', 'No se pudo encontrar el token de usuario.');
        navigation.navigate('Login');
      }
    };
    loadData();
  }, [navigation]);

  const handleSave = async () => {
    setLoading(true);
    try {
      const response = await fetch(`${API_USER_URL}/users/profile`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${userToken}`
        },
        body: JSON.stringify({
          name,
          phoneNumber
        })
      });
      if (response.ok) {
        Alert.alert('Éxito', 'Datos actualizados correctamente');
      } else {
        throw new Error('Error al actualizar los datos del usuario.');
      }
    } catch (error) {
      const errorMessage = error instanceof Error ? error.message : 'Error al actualizar';
      Alert.alert('Error', errorMessage);
    } finally {
      setLoading(false);
      setIsEditing(false);
    }
  };

  if (loading) {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.label}>Nombre:</Text>
      <TextInput
        style={[styles.input, { color: 'black' }]}
        value={name}
        onChangeText={setName}
        editable={isEditing}
      />
      <Text style={styles.label}>Correo electrónico:</Text>
      <TextInput
        style={[styles.input, { color: 'black' }]} 
        value={email}
        editable={false} 
      />
      <Text style={styles.label}>Rol:</Text>
      <TextInput
        style={[styles.input, { color: 'black' }]} 
        value={role}
        editable={false} 
      />
      <Text style={styles.label}>Número de teléfono:</Text>
      <TextInput
        style={styles.input}
        value={phoneNumber}
        onChangeText={setPhoneNumber}
        editable={isEditing}
      />
      {isEditing ? (
        <TouchableOpacity style={styles.button} onPress={handleSave}>
          <Text style={styles.buttonText}>Guardar</Text>
        </TouchableOpacity>
      ) : (
        <TouchableOpacity style={styles.button} onPress={() => setIsEditing(true)}>
          <Text style={styles.buttonText}>Editar Datos</Text>
        </TouchableOpacity>
      )}
    </View>
  );
}

export default ProfileScreen;
