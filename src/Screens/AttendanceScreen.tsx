import React, { useState, useEffect } from 'react';
import { Alert, Text, TouchableOpacity, View, StyleSheet } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as Location from 'expo-location';
import { styles } from '../components/styles/GlobalStyles';
import { AttendanceScreenNavigationProp } from '../navigation/navigationTypes';
import {API_ATTENDANCE_URL} from "@env"

interface AttendanceScreenProps {
  navigation: AttendanceScreenNavigationProp;
}

function AttendanceScreen({ navigation }: { navigation: any }) {
  const [token, setToken] = useState('');
  const [userEmail, setUserEmail] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      const storedToken = await AsyncStorage.getItem('userToken');
      const storedEmail = await AsyncStorage.getItem('userEmail'); 
      setToken(storedToken || '');
      setUserEmail(storedEmail || '');
    };

    fetchData();
  }, []);

  const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  
  const markAttendance = async (type: string) => {
    const isEntry = type === 'entry'; 

    console.log("Correo electrónico a validar:", userEmail);

    if (!userEmail || !emailRegex.test(userEmail)) {
      Alert.alert('Error de Validación', 'Por favor ingresa un correo electrónico válido.');
      return;
    }
    if (!token) {
      Alert.alert('Error de Validación', 'El token no puede estar vacío.');
      return;
    }
    if (typeof isEntry !== 'boolean') {
      Alert.alert('Error de Validación', 'El campo de entrada/salida debe ser booleano.');
      return;
    }

    try {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        Alert.alert('Permiso de ubicación', 'Se requiere permiso para acceder a la ubicación');
        return;
      }

      let location = await Location.getCurrentPositionAsync({});
      const { latitude, longitude } = location.coords;

      const response = await fetch(`${API_ATTENDANCE_URL}/attendance/mark`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
          userEmail: userEmail,
          token: token,
          isEntry: isEntry,
          latitude: latitude,
          longitude: longitude
        }),
      });

      if (!response.ok) {
        const errorData = await response.json();
        console.error("Error response data:", errorData); // Más detalles sobre el error
        throw new Error(errorData.message || JSON.stringify(errorData) || 'Error al marcar asistencia');
      }
      
      Alert.alert('Marcado de asistencia', `Has marcado tu ${type === 'entry' ? 'entrada' : 'salida'} con éxito.`);
    } catch (error) {
      if (error instanceof Error) {
        
        Alert.alert('Error', `No se pudo marcar la asistencia: ${error.message}`);
      } else {
        
        Alert.alert('Error', 'No se pudo marcar la asistencia debido a un error desconocido.');
      }
    }
  };

  const navigateProfile = () => {
    navigation.navigate('Profile');
  };

  const navigateWeeklySummary = () => {
    navigation.navigate('WeeklySummary');
  };

  return (
    <View style={styles.container}>
      <View style={localStyles.buttonGroup}>
        <TouchableOpacity style={[styles.button, localStyles.button]} onPress={() => markAttendance('entry')}>
          <Text style={styles.buttonText}>Marcar entrada</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.button, localStyles.button]} onPress={() => markAttendance('exit')}>
          <Text style={styles.buttonText}>Marcar salida</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.button, localStyles.button]} onPress={navigateProfile}>
          <Text style={styles.buttonText}>Ver perfil de usuario</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.button, localStyles.button]} onPress={navigateWeeklySummary}>
          <Text style={styles.buttonText}>Ver mi resumen semanal</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const localStyles = StyleSheet.create({
  buttonGroup: {
    flexDirection: 'column', 
    justifyContent: 'center', 
    alignItems: 'center', 
  },
  button: {
    width: '80%', 
    height: 50, 
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
  },
});

export default AttendanceScreen;

