import React, { useEffect, useState, useMemo } from 'react';
import { View, Text, TextInput, Button, ScrollView, Alert, StyleSheet, FlatList, TouchableOpacity, Dimensions } from 'react-native';
import { LineChart } from 'react-native-chart-kit';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { fetchWorkers, Worker } from '../utils/fetchWorkers';
import {API_ATTENDANCE_URL} from "@env"

const screenWidth = Dimensions.get("window").width;

const generateColor = (email: string): string => {
    let hash = 0;
    for (let i = 0; i < email.length; i++) {
        hash = email.charCodeAt(i) + ((hash << 5) - hash);
    }
    return `hsl(${hash % 360}, 70%, 70%)`;
};

const getStartAndEndOfWeek = (offset: number) => {
    const now = new Date();
    const firstDay = now.getDate() - now.getDay() + (now.getDay() === 0 ? -6 : 1) + (7 * offset);
    const lastDay = firstDay + 6;

    const startOfWeek = new Date(now.setDate(firstDay));
    startOfWeek.setHours(0, 0, 0, 0);

    const endOfWeek = new Date(now.setDate(lastDay));
    endOfWeek.setHours(23, 59, 59, 999);

    return { startOfWeek: startOfWeek.toISOString(), endOfWeek: endOfWeek.toISOString() };
};

type DayData = {
    Monday: number,
    Tuesday: number,
    Wednesday: number,
    Thursday: number,
    Friday: number,
    Saturday: number,
    Sunday: number
};

type MultiWeeklyData = {
    [email: string]: DayData
};

const WeeklyChartsMultipleScreen = () => {
    const [token, setToken] = useState('');
    const [emails, setEmails] = useState('');
    const [data, setData] = useState<MultiWeeklyData>({});
    const [weekOffset, setWeekOffset] = useState(0);
    const [workers, setWorkers] = useState<Worker[]>([]);
    const [filteredWorkers, setFilteredWorkers] = useState<Worker[]>([]);
    const [searchEmail, setSearchEmail] = useState('');
    const [selectedWorkers, setSelectedWorkers] = useState<Worker[]>([]);

    useEffect(() => {
        const fetchData = async () => {
            const storedToken = await AsyncStorage.getItem('userToken');
            if (storedToken) {
                setToken(storedToken);
                const workersData = await fetchWorkers();
                setWorkers(workersData);
                setFilteredWorkers(workersData);
            }
        };

        fetchData();
    }, []);

    useEffect(() => {
        if (selectedWorkers.length > 0) {
            const emailList = selectedWorkers.map(worker => worker.email).join(',');
            setEmails(emailList);
            fetchData();
        }
    }, [selectedWorkers, weekOffset]);

    const fetchData = async () => {
        if (!emails) {
            Alert.alert('Por favor, ingrese un nuevo email');
            return;
        }

        const { startOfWeek, endOfWeek } = getStartAndEndOfWeek(weekOffset);

        try {
            const response = await fetch(`${API_ATTENDANCE_URL}/attendance/multi-weekly-hours?emails=${emails}&start=${startOfWeek}&end=${endOfWeek}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                }
            });

            if (!response.ok) {
                throw new Error(`HTTP error! status: ${response.status}`);
            }

            const responseData = await response.json();
            if (typeof responseData !== 'object' || responseData === null) {
                throw new Error('La respuesta no es un objeto.');
            }

            setData(responseData);
        } catch (error) {
            const errorMessage = error instanceof Error ? error.message : 'Error desconocido';
            Alert.alert('Error al obtener los datos', errorMessage);
        }
    };

    const chartData = useMemo(() => {
        const labels = ["L", "M", "M", "J", "V", "S", "D"];
        const datasets = Object.keys(data).map(email => ({
            data: Object.values(data[email]),
            color: (opacity = 1) => generateColor(email),
            strokeWidth: 2,
            label: email
        }));
        
        return { labels, datasets };
    }, [data]);

    const handleSelectWorker = (worker: Worker) => {
        setSelectedWorkers(prev => {
            if (prev.find(w => w.email === worker.email)) {
                return prev.filter(w => w.email !== worker.email);
            } else {
                return [...prev, worker];
            }
        });
        setSearchEmail('');
    };

    return (
        <ScrollView>
            <View style={{ padding: 20 }}>
                <Text>Buscar Trabajador por Email:</Text>
                <TextInput
                    style={{ borderColor: 'gray', borderWidth: 1, marginBottom: 10, padding: 10 }}
                    value={searchEmail}
                    onChangeText={setSearchEmail}
                    placeholder="Buscar por email"
                />
                {searchEmail.length > 0 && (
                    <FlatList
                        data={filteredWorkers}
                        keyExtractor={(item) => item.id.toString()}
                        renderItem={({ item }) => (
                            <TouchableOpacity onPress={() => handleSelectWorker(item)}>
                                <Text style={[styles.workerText, selectedWorkers.includes(item) && styles.selectedWorker]}>
                                    {item.email}
                                </Text>
                            </TouchableOpacity>
                        )}
                    />
                )}
                <View style={styles.selectedWorkersContainer}>
                    {selectedWorkers.map(worker => (
                        <Text key={worker.id} style={styles.selectedWorkerText}>
                            {worker.name} ({worker.email})
                        </Text>
                    ))}
                </View>
                <View style={styles.buttonContainer}>
                    <Button title="Buscar" onPress={fetchData} color="#800080" />
                </View>
                <View style={styles.navigationContainer}>
                    <View style={styles.buttonWrapper}>
                        <Button title="Semana Anterior" onPress={() => setWeekOffset(weekOffset - 1)} color="#800080" />
                    </View>
                    <Text style={styles.yearText}>{`Semana ${weekOffset}`}</Text>
                    <View style={styles.buttonWrapper}>
                        <Button title="Siguiente Semana" onPress={() => setWeekOffset(weekOffset + 1)} color="#800080" />
                    </View>
                </View>
                {Object.keys(data).length > 0 && (
                    <LineChart
                        data={chartData}
                        width={screenWidth - 40}
                        height={400}
                        chartConfig={{
                            backgroundColor: '#1cc910',
                            backgroundGradientFrom: '#eff3ff',
                            backgroundGradientTo: '#efefef',
                            decimalPlaces: 1,
                            color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                            labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                            style: {
                                borderRadius: 16
                            },
                            propsForDots: {
                                r: "6",
                                strokeWidth: "2",
                                stroke: "#ffa726"
                            }
                        }}
                        bezier
                        style={{
                            marginVertical: 8,
                            borderRadius: 16
                        }}
                    />
                )}
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    workerText: {
        fontSize: 16,
        padding: 10,
    },
    selectedWorkerText: {
        fontSize: 16,
        fontWeight: 'bold',
        padding: 10,
        marginTop: 10,
        backgroundColor: '#f0f0f0',
        borderRadius: 5,
    },
    selectedWorker: {
        backgroundColor: '#d3d3d3',
    },
    selectedWorkersContainer: {
        marginVertical: 10,
    },
    buttonContainer: {
        marginVertical: 10,
    },
    navigationContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: 10,
    },
    buttonWrapper: {
        flex: 1,
        marginHorizontal: 5,
    },
    yearText: {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
    },
});

export default WeeklyChartsMultipleScreen;

