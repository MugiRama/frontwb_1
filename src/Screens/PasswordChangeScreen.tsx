import React, { useState } from 'react';
import { View, TextInput, TouchableOpacity, Text, Alert } from 'react-native';
import { styles } from '../components/styles/GlobalStyles';
import { PasswordChangeScreenNavigationProp } from '../navigation/navigationTypes';
import {API_USER_URL} from "@env"


interface PasswordChangeScreenProps {
  navigation: PasswordChangeScreenNavigationProp;
}

export default function PasswordChangeScreen({ navigation }: PasswordChangeScreenProps) {
  const [email, setEmail] = useState('');
  const [token, setToken] = useState('');
  const [newPassword, setNewPassword] = useState('');

  const handleChangePassword = async () => {
    try {
      const response = await fetch(`${API_USER_URL}/auth/reset-password`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ token, newPassword }),
      });

      if (response.ok) {
        Alert.alert('Contraseña cambiada', 'Su contraseña ha sido actualizada correctamente.');
        navigation.navigate('Login'); 
      } else {
        throw new Error('Error al cambiar la contraseña');
      }
    } catch (error) {
      const errorMessage = error instanceof Error ? error.message : 'Error desconocido';
      Alert.alert('Fallo en la operación', errorMessage);
    }
  };

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        placeholder="Correo electrónico"
        value={email}
        onChangeText={setEmail}
        keyboardType="email-address"
        autoCapitalize="none"
      />
      <TextInput
        style={styles.input}
        placeholder="Código de 4 dígitos"
        value={token}
        onChangeText={setToken}
        keyboardType="number-pad"
        maxLength={4}
      />
      <TextInput
        style={styles.input}
        placeholder="Nueva contraseña"
        value={newPassword}
        onChangeText={setNewPassword}
        secureTextEntry={true}
      />
      <TouchableOpacity style={styles.button} onPress={handleChangePassword}>
        <Text style={styles.buttonText}>Cambiar contraseña</Text>
      </TouchableOpacity>
    </View>
  );
}
