import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { styles } from '../components/styles/GlobalStyles';
import { ChartsScreenNavigationProp } from '../navigation/navigationTypes';

interface ChartsScreenProps {
  navigation: ChartsScreenNavigationProp;
}

function ChartsScreen({ navigation }: ChartsScreenProps) {
  const goToAnnualChart = () => {
    navigation.navigate('AnnualCharts');
  };
  const goToWeeklyChart = () => {
    navigation.navigate('WeeklyCharts');
  };
  const goToWeeklyChartMultiple = () => {
    
    navigation.navigate('WeeklyChartsMultiple');
  };

  return (
    <View style={styles.container}>
      <Text style={styles.titleText}>Bienvenido a tu sección de Gráficos</Text>
      <TouchableOpacity style={styles.button} onPress={goToAnnualChart}>
        <Text style={styles.buttonText}>Ver Gráficos Anuales</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={goToWeeklyChart}>
        <Text style={styles.buttonText}>Ver Gráficos Semanales </Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={goToWeeklyChartMultiple}>
        <Text style={styles.buttonText}>Ver Gráficos Semanales Múltiple</Text>
      </TouchableOpacity>
    </View>
  );
}

export default ChartsScreen;