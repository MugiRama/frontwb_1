import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, ScrollView, Alert, StyleSheet } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Attendance } from '../types/attendanceTypes';
import {API_ATTENDANCE_URL} from "@env"

const WeeklySummaryScreen: React.FC = () => {
  const [weekOffset, setWeekOffset] = useState<number>(0);
  const [attendances, setAttendances] = useState<{ [date: string]: Attendance[] }>({});
  const [loading, setLoading] = useState<boolean>(false);
  const [email, setEmail] = useState<string | null>(null);

  useEffect(() => {
    const fetchEmail = async () => {
      const storedEmail: string | null = await AsyncStorage.getItem('userEmail');
      if (storedEmail) {
        setEmail(storedEmail);
      } else {
        Alert.alert('Error', 'No se encontró el email del usuario');
      }
    };
    fetchEmail();
  }, []);

  useEffect(() => {
    if (email) {
      fetchAttendances();
    }
  }, [weekOffset, email]);

  const fetchAttendances = async () => {
    setLoading(true);
    const token: string | null = await AsyncStorage.getItem('userToken');
    if (!token) {
      Alert.alert('Error', 'No hay token de usuario almacenado');
      setLoading(false);
      return;
    }

    try {
      const response: Response = await fetch(`${API_ATTENDANCE_URL}/attendance/weekly-summary?weekOffset=${weekOffset}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`
        }
      });

      if (!response.ok) {
        throw new Error('Failed to fetch data');
      }

      const data: { [date: string]: Attendance[] } = await response.json();
      setAttendances(data);
    } catch (error) {
      const errorMessage: string = error instanceof Error ? error.message : 'Error al obtener los datos';
      Alert.alert('Error', errorMessage);
    } finally {
      setLoading(false);
    }
  };

  const navigateWeek = (direction: 'previous' | 'next') => {
    setWeekOffset(prev => direction === 'previous' ? prev - 1 : prev + 1);
  };

  const getWeekDays = (): string[] => {
    const currentDate = new Date();
    currentDate.setUTCHours(0, 0, 0, 0); 
    const dayOfWeek = currentDate.getUTCDay(); 
    const diffToMonday = (dayOfWeek + 6) % 7;

    const weekStart = new Date(currentDate);
    weekStart.setUTCDate(currentDate.getUTCDate() - diffToMonday + (weekOffset * 7)); 

    return Array.from({ length: 7 }, (_, i) => {
      const weekDay = new Date(weekStart);
      weekDay.setUTCDate(weekStart.getUTCDate() + i);
      return weekDay.toISOString().split('T')[0]; 
    });
  };

  const formatDate = (date: string): string => {
    const utcDate = new Date(`${date}T00:00:00Z`);
    const options: Intl.DateTimeFormatOptions = {
      weekday: 'long', 
      year: 'numeric', 
      month: 'long',   
      day: 'numeric',  
      timeZone: 'UTC' 
    };
    return utcDate.toLocaleDateString('es-ES', options);
  };
  
  return (
    <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
      <Text style={styles.titleText}>Resumen Semanal de Asistencia</Text>
      {loading && <Text>Cargando...</Text>}
      {!loading && (
        <View style={styles.table}>
          {getWeekDays().map(day => (
            <View key={day} style={styles.row}>
              <Text style={styles.cell}>
                {formatDate(day)}
              </Text>
              <View style={styles.cell}>
                {attendances[day]?.length ? (
                  attendances[day].map((entry: Attendance, index: number) => (
                    <Text key={index} style={styles.normalText}>
                      Hora: {new Date(entry.timestamp).toLocaleTimeString('es-ES', { hour12: true })} -
                      Tipo: {entry.isEntry ? 'Entrada' : 'Salida'}
                    </Text>
                  ))
                ) : (
                  <Text style={styles.normalText}>No hay registros</Text>
                )}
              </View>
            </View>
          ))}
        </View>
      )}
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.button} onPress={() => navigateWeek('previous')}>
          <Text style={styles.buttonText}>Semana Anterior</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => navigateWeek('next')}>
          <Text style={styles.buttonText}>Siguiente Semana</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    padding: 20,
    alignItems: 'center',
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  table: {
    width: '100%',
    borderWidth: 1,
    borderColor: '#ddd',
  },
  row: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#ddd',
  },
  cell: {
    flex: 1,
    padding: 10,
    justifyContent: 'center',
  },
  normalText: {
    fontSize: 16,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  button: {
    backgroundColor: '#800080',
    padding: 10,
    borderRadius: 5,
    marginHorizontal: 10,
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default WeeklySummaryScreen;
