import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  logo: {
    width: 150,
    height: 150,
    marginBottom: 20,
  },
  titleText: {
    fontSize: 24,
    marginBottom: 20,
  },
  text: {
    textDecorationLine: 'underline',
    color: '#800080',
    fontSize: 24,
  },
  input: {
    width: '100%',
    height: 40,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    paddingHorizontal: 10,
    marginBottom: 10,
  },
  button: {
    backgroundColor: '#800080',
    paddingVertical: 10,
    paddingHorizontal: 30,
    borderRadius: 5,
    marginTop: 10,
    width: '90%',
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    textAlign: 'center',
  },
  linkContainer: {
    marginTop: 10,
  },
  linkText: {
    textDecorationLine: 'underline',
    color: '#800080',
    fontSize: 16,
  },
  label: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 8,
  },
  dayContainer: {
    marginVertical: 8,
  },
  dateText: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  entryText: {
    color: 'green',
  },
  exitText: {
    color: 'red',
  },
  noRecordText: {
    fontStyle: 'italic',
    color: 'gray',
  },
  navigationButtons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  workerButton: {
    backgroundColor: '#800080',
    padding: 15,
    borderRadius: 5,
    marginBottom: 12,
  },
  workerButtonText: {
    color: '#fff',
    fontSize: 16,
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 20
  },
  normalText: {
    fontSize: 16,
    marginVertical: 5
  },
  buttonContainer: {
    marginVertical: 10
  },
  contentContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20
  },
  
  workerText: {
    fontSize: 16,
    padding: 10,
    
  },
  selectedWorkerText: {
    fontSize: 16,
    fontWeight: 'bold',
    padding: 10,
    marginTop: 10,
    backgroundColor: '#f0f0f0',
    borderRadius: 5,
  },
  navigationContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 10,
  },
  buttonWrapper: {
    flex: 1,
    marginHorizontal: 5,
  },
  yearText: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  
  
});
